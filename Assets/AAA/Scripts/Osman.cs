using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Osman : MonoBehaviour
{
    [Header("Osman Character Stats")]
    [SerializeField] private float _health = 100.0f;
    [SerializeField] private float _hunger = 100.0f;
    [SerializeField] private float _thirst = 100.0f;
    [SerializeField] private float _energy = 100.0f;
    [SerializeField] private float _smell = 0.0f;
    [SerializeField] private float _motivation = 100.0f;

    [Header("Osmans' Movement Setting")]
    [SerializeField] private float _walkSpeed = 5.0f;
    [SerializeField] private float _runSpeedMultiplier = 1.0f;

    [Header("Osmans' Sound Settins")]
    [SerializeField] private float _meowDelay = 1f;
    [SerializeField] private float _canMeow = -1f;

    private SpawnManager spawnManager;

    void Start()
    {
        spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
    }

    void Update()
    {
        OsmanMovement();
        OsmanCharm();

    }

    void OsmanMovement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 direction = new Vector3(horizontalInput, transform.position.y, verticalInput);

        // Walk
        // Run
        // Define head direction according to movement!!

        if(Input.GetKey(KeyCode.LeftShift))
        {
            transform.Translate(direction * _walkSpeed * _runSpeedMultiplier * Time.deltaTime);
        }
        else
        {
            transform.Translate(direction * _walkSpeed * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            transform.Rotate(transform.rotation.x, transform.rotation.y + 90f, transform.rotation.z);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            transform.Rotate(transform.rotation.x, transform.rotation.y - 90f, transform.rotation.z);
        }
        //Jump

        //Movement Restrictions
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -4.92f, 4.9f), transform.position.y, Mathf.Clamp(transform.position.z,-4.92f, 4.92f));

    }

    void OsmanCharm()
    {
        if(Input.GetKeyDown(KeyCode.Space) && Time.time > _canMeow)
        {
            _canMeow = Time.time + _meowDelay;
            Debug.Log("Play Sound");
        }
    }

    //Osman'in aldigi damage'a tipine gore random damagelar ver.
    public void OsmanCarDamage()
    {
        float carDamage = Random.Range(20, 100);

        _health = _health - carDamage;

        if (_health < 1)
        {
            //stop spawn manager
            spawnManager.OnPlayerDeath();
            Debug.Log("Osman Died");
        }
    }

}
