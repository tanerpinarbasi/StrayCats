using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [Header("Spawn Prefabs")]
    [SerializeField] private GameObject _trashCan;

    [Header("Spawn Routines")]
    [SerializeField] private float _trashCanRoutine = 30.0f;

    [Header("Containers")]
    [SerializeField] private Transform _Container;

    private bool isOsmanDead = false;



    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TrashCanSpawnRoutine());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator TrashCanSpawnRoutine()
    {
        while(isOsmanDead == false)
        {
            Vector3 posToSpawn = new Vector3(-0.7f, 0.24f, -0.66f);
            Debug.Log("Trash can instantiate");
            GameObject newTrashCan = Instantiate(_trashCan, posToSpawn, Quaternion.identity);
            newTrashCan.transform.parent = _Container.transform;
            yield return new WaitForSeconds(_trashCanRoutine);
        }
        
    }

    public void OnPlayerDeath()
    {
        isOsmanDead = true;
    }
}
