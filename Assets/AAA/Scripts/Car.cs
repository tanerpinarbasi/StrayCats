using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    [SerializeField] private float _carSpeed =5.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Saate ba�l� trafik yap
        //Kediyi g�r�nce random bir �ekilde korna �als�nlar.
        CarMovement();

    }

    void CarMovement()
    {

        Vector3 carDirectionRight = new Vector3(1, 0, 0);
        transform.Translate(carDirectionRight * _carSpeed * Time.deltaTime);


        if (transform.position.x >= 9.5)
        {
            //Find an appropriate naming.
            Vector3 newPosition = new Vector3(5.9f, 0.31f, -3.3f);

            transform.Rotate(0, 180, 0);
            transform.position = newPosition;
            transform.Translate(carDirectionRight * -1 * _carSpeed * Time.deltaTime);
        }
        else if (transform.position.x <= -8f)
        {
            Vector3 newPosition = new Vector3(-5.85f, 0.31f, -4.36f);

            transform.Rotate(0, 180, 0);
            transform.position = newPosition;
            transform.Translate(carDirectionRight * _carSpeed * Time.deltaTime);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Debug.Log("Hit by: " + other.tag);

            Osman osman = other.transform.GetComponent<Osman>();

            if(osman != null)
            {
                osman.OsmanCarDamage();
            }
        }
    }
}
