using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashCan : MonoBehaviour
{
    [SerializeField] private float _trashCanDuration = 15.0f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TrashCanDestroyRoutine());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator TrashCanDestroyRoutine()
    {
        yield return new WaitForSeconds(_trashCanDuration);
        Destroy(this.gameObject);
    }
}
